# Three JS Programming Execise

Thanks for your interest in joining Desupervised!

This is a small programming exercise to determine how well you deal with
challenges and can use materials at your disposal to solve UI-related tasks.

In this exercise we'd like you to build a simple web app that can visualise a 3D
model. The user should be able to move the model around by clicking and dragging
with the mouse.

The model we'd like you to use is an `.obj` file:
[male02.obj](static/male02.obj)

You're encouraged to use [Three.js](https://threejs.org/) but you may attempt
the task with another framework if you're more familiar with something else.

Bonuses:
- Containerize your solution in a Docker container
- Add zoom in/out by scrolling
- Add buttons to reset model position and zoom

We're just as interested in how you approach the task and document what you find
or what you struggle with as we are with a working solution. So please don't
worry too much if you don't get a final working solution.

The task should take no more than 3 hours (you're welcome to spend longer on it,
but please try to submit a solution within 3 working days).

Please submit your solution as a link to a git repository owned by you. You can
send the link to [johan@desupervised.io](mailto:johan@desupervised.io).

Good luck!
